## Slync Programming Test - Twitter Client

Implement a simple *Twitter client* as a single page application which initially shows general public tweets. Allow the user to specify a Twitter handle to view the tweets from. When a user requests a specific Twitter handle and the UI shows the latest tweets from that handle, there should also be a way to filter / search tweets by text. Also, once the user has selected a handle to view the Tweets from, the UI should periodically update with newer Tweets.

### Grading Criteria

* UI Design
* Architecture
* Maintainability
* Testing

### Technology

You can pick any frontend and/or backend technologies you are familiar with. You can and should use the Twitter API.

### Submission

You will submit to the private git repo we have added you to. When done, make sure you provide instructions on how to run your code (preferably as a docker image) in the README.

Note: *Do not* create a Twitter clone and *do not* share any Twitter API secrets in your code.

Best of luck!
Slync Engineering Team

### How to run application locally

```sh
npm install
npm run build
npm start -- TWITTER_CONSUMER_KEY [TWITTER_CONSUMER_KEY] TWITTER_CONSUMER_SECRET [TWITTER_CONSUMER_SECRET]

or you can set environment variables with the same names and run "npm start" without params
```

### For development
```sh
// start nodejs server
cd backend
npm start -- TWITTER_CONSUMER_KEY [TWITTER_CONSUMER_KEY] TWITTER_CONSUMER_SECRET [TWITTER_CONSUMER_SECRET]

// serve frontend
cd frontend
npm start

api request will be proxied to the backend server
```

### Build and run Docker image
```sh
docker build -t [DOCKER_IMAGE_NAME] .

//run docker image
docker run --env TWITTER_CONSUMER_KEY=[TWITTER_CONSUMER_KEY] --env TWITTER_CONSUMER_SECRET=[TWITTER_CONSUMER_SECRET] --env SERVER_PORT=[SERVER_PORT] -p [PUBLIC_PORT]:[SERVER_PORT] --expose=[SERVER_PORT] -d [DOCKER_IMAGE_NAME]

//run http://localhost:[PUBLIC_PORT]
```

