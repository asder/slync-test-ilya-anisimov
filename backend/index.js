const express = require('express');
const path = require('path');
const {SERVER_PORT} = require('./config');
const {twitterRequest} = require('./twitter-client')

const app = express();

app.get('/api/search', async (req, res) => {
    const params = {};
    req.query.q && (params.q = req.query.q);
    req.query.maxId && (params.max_id = req.query.maxId);
    const response = await twitterRequest('get', 'search/tweets', params);
    res.send(response);
});

app.get('/api/user-timeline', async (req, res) => {
    const params = {};
    req.query.screenName && (params.screen_name = req.query.screenName);
    req.query.maxId && (params.max_id = req.query.maxId);
    req.query.sinceId && (params.since_id = req.query.sinceId);
    const response = await twitterRequest('get', 'statuses/user_timeline', params);
    res.send(response);
});

app.get('/api/user-show', async (req, res) => {
    const params = {};
    req.query.tweeterId && (params.screen_name = req.query.tweeterId);
    const response = await twitterRequest('get', 'users/show', params);
    res.send(response);
});

const staticPath = path.join(__dirname, '..', 'frontend', 'build');
app.get('*', express.static(staticPath));

const indexPath = path.join(staticPath, 'index.html');
app.get('*', (req, res, next) => {
    res.sendFile(indexPath);
 });

app.listen(SERVER_PORT, () => console.log(`Server is listening at port:${SERVER_PORT}`));

    
