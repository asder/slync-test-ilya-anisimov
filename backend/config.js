const args = {};
for(let i=2; i<process.argv.length; i+=2) {
    args[process.argv[i]] = process.argv[i + 1];
}

module.exports = Object.freeze({
    SERVER_PORT: process.env.SERVER_PORT || args.SERVER_PORT || 4000,
    TWITTER_CONSUMER_KEY: process.env.TWITTER_CONSUMER_KEY || args.TWITTER_CONSUMER_KEY,
    TWITTER_CONSUMER_SECRET: process.env.TWITTER_CONSUMER_SECRET || args.TWITTER_CONSUMER_SECRET
});