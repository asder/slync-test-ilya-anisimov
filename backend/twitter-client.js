const Twitter = require('twitter-lite');
const {TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET} = require('./config');
const INVALID_EXPIRED_TOKEN_CODE = 89;

let twClient;

async function twitterRequest(method, ...props) {
    let response;
    try {
        try {
            if(!twClient) {
                twClient = await initTwitterClient();
            }
            response = await twClient[method](...props);
        } catch(e) {
            if(e.errors && e.errors instanceof Array) {
                if(e.errors.find((e) => e.code === INVALID_EXPIRED_TOKEN_CODE)) {
                    try {
                        twClient = await initTwitterClient();
                        response = await twClient[method](...props);
                    } finally {}
                } else {
                    return {success: false, errors: e.errors};
                }
            } else {
                throw e;
            }
        }
    } catch(e) {
        console.log(e);
        return {success: false, errors: [e.name]};
    }
    return {success: true, response};
}

function initTwitterClient() {
    const authClient = new Twitter({
        consumer_key: TWITTER_CONSUMER_KEY,
        consumer_secret: TWITTER_CONSUMER_SECRET
    });
    return authClient.getBearerToken()
        .then((resp) => {
            return new Twitter({
                bearer_token: resp.access_token
            });
        });
}

module.exports = {
    twitterRequest
};