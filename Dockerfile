FROM node:10

RUN mkdir -p /usr/src/app/backend && mkdir -p /usr/src/app/frontend && mkdir -p /tmp/frontend

COPY ./backend/package*.json /usr/src/app/backend

RUN cd /usr/src/app/backend && npm install

COPY ./frontend/package*.json /tmp/frontend

RUN cd /tmp/frontend && npm install

WORKDIR /usr/src/app

COPY ./backend/. /usr/src/app/backend

COPY ./frontend/. /tmp/frontend

RUN cd /tmp/frontend && npm run build && cp -a /tmp/frontend/build /usr/src/app/frontend

CMD [ "node", "/backend/index.js" ]