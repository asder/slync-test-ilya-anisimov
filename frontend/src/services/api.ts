export class Api {

    public fetch(url: string, options: any = {}): Promise<any> {
        if(!options.headers) {
            options.headers = {};
        }
        if(!options.headers['Content-Type']) {
            options.headers['Content-Type'] = 'application/json; charset=utf-8';

            if(options.body && typeof options.body !== 'string') {
                options.body = JSON.stringify(options.body);
            }
        }
        options.headers = new Headers(options.headers);
        return fetch(url, options)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if(json.success) {
                    return json.response;
                }
                return Promise.reject(json);
            });
    }
}

export const toQueryString = (obj: any) => {
    return Object.keys(obj)
        .reduce((result: any, key: any) => {
            if(obj[key] !== undefined) {
                result.set(key, obj[key]);
            }
            return result;
        }, new URLSearchParams())
        .toString();
};
