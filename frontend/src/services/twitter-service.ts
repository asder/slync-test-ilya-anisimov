import { Api, toQueryString } from './api';

class TwitterService extends Api {
    search(params: {q: string, maxId?: BigInt}): Promise<any> {
        return this.fetch(`/api/search?${toQueryString(params)}`)
            .then(result => result.statuses);
    }

    userTimeline(params: {screenName: string, maxId?: BigInt, sinceId?: BigInt}): Promise<any> {
        return this.fetch(`/api/user-timeline?${toQueryString(params)}`)
            
    }

    getUser(params: {tweeterId: string}): Promise<any> {
        return this.fetch(`/api/user-show?${toQueryString(params)}`);
    }
}

export const twitterService = new TwitterService();