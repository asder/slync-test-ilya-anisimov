import React from 'react';
import {Link} from 'react-router-dom';

export const twitterTextToComponents = (txt: string = '') => {
    const matches = txt.match(/(?:@\w+)|(?:#\w+)|(https?:\/\/[^\s]+)/ig);
    const text = [txt];
    if(matches) {
        const matchSet = new Set(matches);
        matchSet.forEach((match: any) => {
            let matchType;
            switch(match.charAt(0)) {
                case '@':
                    matchType = 'twitter';
                    break;
                case '#':
                    matchType = 'hashtag';
                    break;
                default:
                    matchType = match.indexOf('http') === 0 ? 'link' : undefined;
            }
            for(let i=0; i<text.length;) {
                if(typeof text[i] !== 'string') {
                    i++;
                    continue;
                }
                const subText = text[i].split(match);
                for(let j=0; j<subText.length - 1; j+=2) {
                    let component;
                    switch(matchType) {
                        case 'twitter':
                            component = (<Link key={Math.random()} className="tweet__link" to={`/tweeter/${match.slice(1)}`}>{match}</Link>);
                            break;
                        case 'hashtag':
                            component = (<Link key={Math.random()} className="tweet__link" to={`/search?q=${encodeURIComponent(match)}`}>{match}</Link>);
                            break;
                        case 'link':
                            component = (<a key={Math.random()} className="tweet__link" target="_blank" href={match}>{match}</a>)
                    }
                    // @ts-ignore
                    subText.splice(i + 1, 0, component);
                }
                text.splice(i, 1, ...subText);
                i += subText.length;
            }
        });
    }
    return text;
};