import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import {App} from './components/app';

setTimeout(() => {
  document.getElementsByClassName('launch-logo')[0].classList.add('launch-logo_loaded');

  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById('root')
  );
}, 1000);
