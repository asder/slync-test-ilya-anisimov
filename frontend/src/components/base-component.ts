import React from 'react';

const listeners = new Map();
const events: {[key: string]: {killId: any, params: any}} = {};

export class BaseComponent extends React.Component<{[key: string]: any}> {

    public isEventNow(eventName: string) {
        return events.hasOwnProperty(eventName);
    }

    public on(eventName: string, callback: Function) {
        if(!listeners.has(eventName)) {
            listeners.set(eventName, new Map());
        }
        const map = listeners.get(eventName);
        if(!map.has(this)) {
            map.set(this, new Set());
        }
        const set = map.get(this);
        set.add(callback);

        if(this.isEventNow(eventName)) {
            callback(events[eventName].params);
        }
    }

    public emit(eventName: string, params?: any, delay?: number) {
        if(listeners.has(eventName)) {
            const map = listeners.get(eventName);
            map.forEach((set: Set<Function>) => {
                set.forEach((key: Function, callback: Function) => {
                    callback(params);
                });
            });
        }

        if(this.isEventNow(eventName)) {
            clearTimeout(events[eventName].killId);
            delete events[eventName];
        }

        if(delay) {
            events[eventName] = {
                killId: setTimeout(() => {
                    delete events[eventName];
                }, delay),
                params
            };
        }
    }

    public componentWillUnmount() {
        listeners.forEach((map: Map<Object, Set<Function>>, eventName: string) => {
            map.delete(this);
            if(map.size === 0) {
                listeners.delete(eventName);
            }
        });
    }
}