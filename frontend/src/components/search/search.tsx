import React from 'react';
import {BaseComponent} from '../base-component';
import {Timeline} from '../timeline';
import {twitterService} from '../../services';
import './search.scss';
import { debounce } from 'lodash';

declare const document: any;

export class Search extends BaseComponent {
    private maxId: BigInt | undefined;
    private scrollListener: any;

    state = {
        tweets: []
    }

    async componentDidMount() {
        await this.fetchTweets();

        this.scrollListener = (() => {
            const listener = async() => {
                const {scrollingElement} = document;
                if(scrollingElement.scrollHeight - scrollingElement.scrollTop - 100 <= scrollingElement.clientHeight) {
                    window.removeEventListener('scroll', listener);
                    await this.fetchTweets();
                    window.addEventListener('scroll', listener);
                }
            };
            window.addEventListener('scroll', listener);
            return {
                remove: () => {
                    window.removeEventListener('scroll', listener);
                }
            };
        })();
    }

    componentDidUpdate(prevProps: any) {
        if (this.props.location !== prevProps.location) {
            this.scrollListener.remove();
            this.maxId = undefined;
            this.state.tweets = [];
            this.setState(this.state);
            this.componentDidMount();
        }
    }
    
    render() {
        return (
            <div>
                <div className="search">
                    <div className="search__logo logo__image"/>
                    <input className="search__input" type="text" placeholder="Search for tweets" onChange={this.handleSearchInputChange}/>
                </div>
                <div className="search__timeline">
                    {this.state.tweets.length > 0 && <Timeline tweets={this.state.tweets}/>}
                </div>
            </div>
        );
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        this.scrollListener.remove();
    }

    private handleSearchInputChange = ({target: {value}}: any) => {
        this.changeSearchUrl({q: value});
    };

    private changeSearchUrl = debounce(({q}) => {
        q = q || 'general';
        this.props.history.push(`${this.props.location.pathname}?q=${q}`);
    }, 300, {
        leading: true,
        trailing: true
    });

    private async fetchTweets() {
        const query = new URLSearchParams(this.props.location.search);
        try {
            const tweets = await twitterService.search({q: query.get('q') as string, maxId: this.maxId});
            if(tweets.length) {
                this.maxId = BigInt(tweets[tweets.length - 1].id_str) - 1n;
                // @ts-ignore
                this.state.tweets.push(...tweets);
                this.forceUpdate();
            }
        } catch(e) {
            console.log(e);
        }
    }
}