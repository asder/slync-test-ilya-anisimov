import React from 'react';
import './app.scss';
import {BaseComponent} from '../base-component';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import {Search} from '../search';
import {Twitter} from '../twitter';

export class App extends BaseComponent {

  render() {
    return (
      <div className="app">
        <Router>
          <Route exact path='/'>
            <Redirect to="/search?q=general"/>
          </Route>
          <Route exact path='/search' component={Search}/>
          <Route path='/tweeter/:tweeterId' component={Twitter}/>
        </Router>
      </div>
    );  
  }
}