import React from 'react';
import {Link} from 'react-router-dom';
import {BaseComponent} from '../base-component';
import {Timeline} from '../timeline';
import {twitterService} from '../../services';
import './twitter.scss';
import {twitterTextToComponents} from '../../utils';

declare const document: any;

export class Twitter extends BaseComponent {
    private maxId: BigInt | undefined;
    private sinceId: BigInt | undefined;
    private scrollListener: any;
    private fetchTimeoutId: any;

    state: any = {
        tweets: [],
        twitter: {}
    };

    async componentDidMount() {
        this.fetchUser();
        await this.fetchNewTweets();
        
        this.scrollListener = (() => {
            const listener = async() => {
                const {scrollingElement} = document;
                if(scrollingElement.scrollHeight - scrollingElement.scrollTop - 100 <= scrollingElement.clientHeight) {
                    window.removeEventListener('scroll', listener);
                    await this.fetchTweets();
                    window.addEventListener('scroll', listener);
                }
            };
            window.addEventListener('scroll', listener);
            return {
                remove: () => {
                    window.removeEventListener('scroll', listener);
                }
            };
        })();
    }

    componentDidUpdate(prevProps: any) {
        if (this.props.location !== prevProps.location) {
            this.scrollListener.remove();
            clearTimeout(this.fetchTimeoutId);
            this.sinceId = undefined;
            this.maxId = undefined;
            this.state = {tweets: [], twitter: {}};
            this.setState(this.state);
            this.componentDidMount();
        }
    }

    render() {
        const {twitter} = this.state;
        const description = twitterTextToComponents(twitter.description);
        return (
            <div>
                <div className="twitter">
                    <Link to={'/search?q=general'} className="twitter__logo logo__image"/>
                    <div className="twitter__description">{twitter.name} <br/> {description}</div>
                </div>
                <div className="twitter twitter_fake">
                    <Link to={'/search?q=general'} className="twitter__logo logo__image"/>
                    <div className="twitter__description">{twitter.name} <br/> {description}</div>
                </div>
                <div className="twitter__timeline">
                    {this.state.tweets.length > 0 && <Timeline tweets={this.state.tweets}/>}
                </div>
            </div>
        );
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        this.scrollListener.remove();
        clearTimeout(this.fetchTimeoutId);
    }

    private async fetchTweets() {
        try {
            const tweets = await twitterService.userTimeline({screenName: this.props.match.params.tweeterId, maxId: this.maxId});
            !this.sinceId && (this.sinceId = BigInt(tweets[0].id_str));
            this.maxId = BigInt(tweets[tweets.length - 1].id_str) - 1n;
            // @ts-ignore
            this.state.tweets.push(...tweets);
            this.forceUpdate();
        } catch(e) {
            console.log(e);
        }
    }

    private async fetchNewTweets() {
        try {
            const tweets = await twitterService.userTimeline({screenName: this.props.match.params.tweeterId, sinceId: this.sinceId});
            if(tweets.length) {
                this.sinceId = BigInt(tweets[0].id_str);
                !this.maxId && (this.maxId = BigInt(tweets[tweets.length - 1].id_str) - 1n);
                // @ts-ignore
                this.state.tweets.unshift(...tweets);
                this.forceUpdate();
            }
        } catch(e) {
            console.log(e);
        }
        this.fetchTimeoutId = setTimeout(() => {
            this.fetchNewTweets();
        }, 10000);
    }

    private fetchUser() {
        const {tweeterId} = this.props.match.params;
        twitterService.getUser({tweeterId})
            .then((twitter) => {
                this.setState({twitter});
            })
            .catch(console.log);
    }
}