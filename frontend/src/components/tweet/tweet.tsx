import React from 'react';
import {Link} from 'react-router-dom';
import {BaseComponent} from '../base-component';
import './tweet.scss';
import {twitterTextToComponents} from '../../utils';

export class Tweet extends BaseComponent {
    state: any = {
        text: [],
        user: {}
    };
    private prevTweet: any;

    componentDidMount() {
        this.extractTweetData(this.props.tweet);
    }
    
    componentDidUpdate(prevProps: any) {
        if (this.props.tweet !== prevProps.tweet) {
            this.extractTweetData(this.props.tweet);
        }
    }

    render() {
        const {text, user} = this.state;
        return (
            <div className="tweet">
                <div className="tweet__avatar-wrapper">
                    <Link to={`/tweeter/${user.screen_name}`} className="tweet__avatar" style={{backgroundImage: `url(${user.profile_image_url_https})`}}/>
                </div>
                <div className="tweet__content">
                    <Link to={`/tweeter/${user.screen_name}`} className="tweet__content-author">
                        <span className="tweet__content-author-name">{user.name}</span>
                        <span className="tweet__content-author-tweeter">@{user.screen_name}</span>
                    </Link>
                    {text}
                </div>
            </div>
        );
    }

    private extractTweetData(tweet: any) {
        const {user, text} = tweet;
        this.setState({text: twitterTextToComponents(text), user});
    }

}