import React from 'react';
import {BaseComponent} from '../base-component';
import {Tweet} from '../tweet';
import './timeline.scss';

export class Timeline extends BaseComponent {

    render() {
        return (
            <>
                {this.props.tweets.map((tweet: any, key: number) => (<Tweet key={key} tweet={tweet}/>))}
            </>
        );
    }
}